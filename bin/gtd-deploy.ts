#!/usr/bin/env node
import 'source-map-support/register';
import * as cdk from 'aws-cdk-lib';
import { GtdDeployStack } from '../lib/gtd-deploy-stack';
import { Utils } from '../lib/utils';
import { Tags } from 'aws-cdk-lib';

const app = new cdk.App();
const config = Utils.getConfig(app);

const stack = new GtdDeployStack(app, `GtdStack-${config.env}` , config, {
  env: {
    account: process.env.CDK_DEFAULT_ACCOUNT,
    region: process.env.CDK_DEFAULT_REGION
  }
});

Tags.of(stack).add('stack', 'gtd');
Tags.of(stack).add('manager', 'cdk');
Tags.of(stack).add('env', config.env);
