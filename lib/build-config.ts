export interface BuildConfig {
  env: 'dev' | 'prod',
  buildDb: boolean;
}
