import * as cdk from 'aws-cdk-lib';
import { Construct } from 'constructs';
import { BuildConfig } from './build-config';
// import * as sqs from 'aws-cdk-lib/aws-sqs';

export class GtdDeployStack extends cdk.Stack {
  constructor(scope: Construct, id: string, config: BuildConfig, props?: cdk.StackProps) {
    super(scope, id, props);
  }
}
