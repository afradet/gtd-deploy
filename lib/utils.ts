import { App } from 'aws-cdk-lib';
import { BuildConfig } from './build-config';
import * as fs from 'fs'
import * as path from "path";
const yaml = require('js-yaml');

export class Utils {

  static getConfig(app: App) {
    let env = app.node.tryGetContext('env');
    if (!env)
      throw new Error("Context variable missing on CDK command. Pass in as `-c config=XXX`");

    let rawConfig = yaml.load(fs.readFileSync(path.resolve("./env/"+env+".yaml"), "utf8"));

    let buildConfig: BuildConfig = {
      env,
      buildDb: this.validateBool(rawConfig, 'buildDb')
    };

    return buildConfig;
  }

  static validateString(object: { [name: string]: any }, propName: string): string {
    if (!object[propName] || object[propName].trim().length === 0)
      throw new Error(propName + " does not exist or is empty");

    return object[propName];
  }

  static validateBool(object: { [name: string]: any }, propName: string): boolean {
    if (!object[propName] || object[propName].trim().length === 0)
      return false;

    return object[propName] === 'true';
  }
}
